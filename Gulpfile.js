var gulp = require('gulp'),
sass = require('gulp-sass')
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
cmq = require('gulp-group-css-media-queries'),
cssmin = require('gulp-cssmin'),
rename = require("gulp-rename"),
autoprefixer = require('gulp-autoprefixer'),
sourcemaps = require('gulp-sourcemaps'),
es6transpiler = require('gulp-es6-transpiler'),
browserSync = require('browser-sync').create();
connect = require('gulp-connect-php');


gulp.task('serve', function() {
  connect.server(); // Remove this when starting theme development as will be using nginx
  
  browserSync.init({
    proxy: 'http://localhost:8000' // Change this to be WP URL when starting theme development
  });

  gulp.watch(['**/*.php']).on('change', function () {
    browserSync.reload();
  });
  gulp.watch(['css/main.css', 'js/app.js']).on('change', function () {
    browserSync.reload('css/main.css', 'js/app.js');
  });

  gulp.watch('css/src/**/*.scss', ['styles']);
  gulp.watch('js/src/*.js', ['scripts']);
  
});
gulp.task('disconnect', function() {
    connect.closeServer();
});

gulp.task('scripts', function () {

    // Edit the source to add addition javascript files that will compile into plugin.min.js
    gulp.src('js/src/*.js')
        .pipe(sourcemaps.init())
        .pipe(es6transpiler())
        .on('error', swallowError)
        .pipe(concat('app.js')) //the name of the resulting file
        .pipe(sourcemaps.write('./'))
        .on('error', swallowError)
        .pipe(gulp.dest('js'));

      gulp.src('js/src/*.js')
        .on('error', swallowError) //select all javascript files under js/ and any subdirectory
        .pipe(es6transpiler())
        .on('error', swallowError)
        .pipe(concat('app.js')) //the name of the resulting file
        .on('error', swallowError) // Swallow errors so doesn't stop running
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({})) // Minify
        .on('error', swallowError)
        .pipe(gulp.dest('js')) //Output directory
});

gulp.task('combine', function () {

    // Edit the source to add addition javascript files that will compile into plugin.min.js
    gulp.src(['js/hammer.js', 'js/app.js'])
        .pipe(sourcemaps.init())
        .on('error', swallowError)
        .pipe(concat('combine.js')) //the name of the resulting file
        .pipe(sourcemaps.write('./'))
        .on('error', swallowError)
        .pipe(gulp.dest('js'));

    gulp.src(['js/hammer.js', 'js/app.min.js'])
        .pipe(sourcemaps.init())
        .on('error', swallowError)
        .pipe(concat('combine.js')) //the name of the resulting file
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./'))
        .on('error', swallowError)
        .pipe(gulp.dest('js'));
});



gulp.task('styles', function () {
    gulp.src('css/src/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass())
      .on('error', swallowError)
      .pipe(autoprefixer({
          browsers: ['> 5%'],
          cascade: true
      }))
      .pipe(sourcemaps.write('./'))
      .on('error', swallowError)
      .pipe(gulp.dest('css'));

    gulp.src('css/src/*.scss')
      .pipe(sass())
      .on('error', swallowError)
      .pipe(autoprefixer({
          browsers: ['> 5%'],
          cascade: true
      }))
      .pipe(cmq())
      .pipe(cssmin())
      .pipe(rename({suffix: '.min'}))
      .on('error', swallowError)
      .pipe(gulp.dest('css'));
});

gulp.task('default',['scripts','styles'], function(){});

gulp.task('watch', function() {
  gulp.watch('css/src/**/*.scss', ['styles']);
  gulp.watch('js/src/*.js', ['scripts']);
});

// Custom error function to stop emit(end);
var swallowError = function(error) {
    console.log(error.toString());
}